Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: LGPL-2.1+

Files: debian/*
Copyright: © 2011-2023 Alberto Garcia <berto@igalia.com>
License: LGPL-2.1+

Files: help/*
Copyright: 2018-2022, grilo-pluginss COPYRIGHT HOLDER
License: GFDL-1.2+

Files: help/C/*
Copyright: no-info-found
License: GFDL-1.2+

Files: help/C/legal.xml
Copyright: no-info-found
License: GFDL-1.1+

Files: help/es/*
Copyright: C/grilo-plugins.xml:35
License: GFDL-1.2+

Files: help/eu/*
Copyright: 2021, grilo-pluginss COPYRIGHT HOLDER
License: GFDL-1.2+ and/or LGPL-2.1+

Files: help/examples/example-tmdb.c
Copyright: 2012, Canonical Ltd.
License: LGPL-2.1+

Files: help/hu/*
Copyright: 2022, Free Software Foundation, Inc.
License: GFDL-1.2+

Files: help/pl/*
Copyright: 2018-2020, the grilo-plugins authors.
License: GFDL-1.2+

Files: meson.build
Copyright: 2016, Igalia S.L.
License: LGPL-2.1

Files: src/*
Copyright: 2010-2012, 2014, 2018, Igalia S.L.
License: LGPL-2.1+

Files: src/bookmarks/*
Copyright: 2014, 2015, Bastien Nocera <hadess@hadess.net>
License: LGPL-2.1+

Files: src/bookmarks/grl-bookmarks.c
Copyright: 2014, Bastien Nocera <hadess@hadess.net>
 2010, 2011, Igalia S.L.
License: LGPL-2.1+

Files: src/bookmarks/grl-bookmarks.h
Copyright: 2010-2012, 2014, 2018, Igalia S.L.
License: LGPL-2.1+

Files: src/chromaprint/*
Copyright: 2014-2016, 2018, 2019, Grilo Project
License: LGPL-2.1+

Files: src/dleyna/*
Copyright: 2011, 2013, Intel Corporation.
License: LGPL-2.1+

Files: src/dleyna/grl-dleyna-source.c
 src/dleyna/grl-dleyna.c
Copyright: 2011, 2013, Intel Corporation.
 2010-2012, Igalia S.L.
License: LGPL-2.1+

Files: src/dleyna/grl-dleyna-source.h
Copyright: 2013, Intel Corp.
 2010, Igalia S.L.
License: LGPL-2.1+

Files: src/dleyna/grl-dleyna-utils.c
 src/dleyna/grl-dleyna-utils.h
Copyright: 2014, Giovanni Campagna <scampa.giovanni@gmail.com>
License: LGPL-2.1+

Files: src/dmap/*
Copyright: 2008, W. Michael Petullo <mike@flyn.org>
License: LGPL-2.1+

Files: src/dmap/grl-common.c
 src/dmap/grl-daap.c
 src/dmap/grl-dpap.c
Copyright: 2012, Igalia S.L.
 2011, W. Michael Petullo.
License: LGPL-2.1+

Files: src/dmap/grl-common.h
 src/dmap/grl-daap.h
 src/dmap/grl-dpap.h
Copyright: 2012, Igalia S.L.
 2011, W. Michael Petullo
License: LGPL-2.1+

Files: src/dmap/grl-daap-compat.h
 src/dmap/grl-dmap-compat.h
 src/dmap/grl-dpap-compat.h
Copyright: 2019, W. Michael Petullo
 2019, Igalia S.L.
License: LGPL-2.1+

Files: src/dmap/grl-daap-db.c
 src/dmap/grl-daap-record.c
 src/dmap/grl-dpap-db.c
 src/dmap/grl-dpap-record.c
Copyright: 2012, W. Michael Petullo.
License: LGPL-2.1+

Files: src/filesystem/grl-filesystem.c
Copyright: 2011, 2013, Intel Corporation.
 2010-2012, Igalia S.L.
License: LGPL-2.1+

Files: src/filesystem/grl-filesystem.h
Copyright: 2010-2012, 2014, 2018, Igalia S.L.
License: LGPL-2.1+

Files: src/flickr/flickr-oauth.c
Copyright: 2021, Marek Chalupa <mchalupa@redhat.com>
License: LGPL-2.1

Files: src/flickr/flickr-oauth.h
Copyright: no-info-found
License: LGPL-2.1+

Files: src/flickr/gflickr.c
Copyright: 2016, Rafael Fonseca <r4f4rfs@gmail.com>
 2013, Marek Chalupa <mchalupa@redhat.com>
 2011-2013, Juan A. Suarez Romero <jasuarez@igalia.com>
License: LGPL-2.1

Files: src/flickr/grl-flickr.c
Copyright: 2011, 2013, Intel Corporation.
 2010-2012, Igalia S.L.
License: LGPL-2.1+

Files: src/freebox/*
Copyright: 2013, Bastien Nocera <hadess@hadess.net>
 2011, Intel Corp
License: LGPL-2+

Files: src/freebox/grl-freebox.c
 src/freebox/grl-freebox.h
Copyright: 2012-2014, Bastien Nocera
License: LGPL-2.1+

Files: src/gravatar/grl-gravatar.c
Copyright: 2012, Canonical Ltd.
 2010, 2011, Igalia S.L.
License: LGPL-2.1+

Files: src/gravatar/grl-gravatar.h
Copyright: 2010-2012, 2014, 2018, Igalia S.L.
License: LGPL-2.1+

Files: src/lua-factory/*
Copyright: 2013-2016, Victor Toso.
License: LGPL-2.1+

Files: src/lua-factory/lua-library/*
Copyright: 1999, Helix Code, Inc.
License: LGPL-2+

Files: src/lua-factory/lua-library/inspect.lua
Copyright: 2013, Enrique García Cota
License: Expat

Files: src/lua-factory/lua-library/lua-json.c
 src/lua-factory/lua-library/lua-libraries.h
Copyright: 2013-2016, Victor Toso.
License: LGPL-2.1+

Files: src/lua-factory/lua-library/lua-xml.c
Copyright: 2014, 2015, Bastien Nocera <hadess@hadess.net>
License: LGPL-2.1+

Files: src/lua-factory/sources/*
Copyright: 2014-2016, 2018, 2019, Grilo Project
License: LGPL-2.1+

Files: src/lua-factory/sources/grl-euronews.lua
 src/lua-factory/sources/grl-guardianvideos.lua
 src/lua-factory/sources/grl-radiofrance.lua
Copyright: 2012-2014, Bastien Nocera
License: LGPL-2.1+

Files: src/lua-factory/sources/grl-lastfm-cover.lua
Copyright: 2015, Bastien Nocera.
License: LGPL-2.1+

Files: src/lua-factory/sources/grl-musicbrainz-coverart.lua
Copyright: 2013-2016, Victor Toso.
License: LGPL-2.1+

Files: src/lua-factory/sources/grl-theaudiodb-cover.lua
Copyright: 2018, The Grilo Developers.
License: LGPL-2.1+

Files: src/magnatune/*
Copyright: 2013-2016, Victor Toso.
License: LGPL-2.1+

Files: src/opensubtitles/*
Copyright: 2014, 2015, Bastien Nocera <hadess@hadess.net>
License: LGPL-2.1+

Files: src/optical-media/*
Copyright: 2012-2014, Bastien Nocera
License: LGPL-2.1+

Files: src/raitv/*
Copyright: no-info-found
License: LGPL-2.1+

Files: src/thetvdb/*
Copyright: 2013-2016, Victor Toso.
License: LGPL-2.1+

Files: src/tmdb/*
Copyright: 2012, Canonical Ltd.
License: LGPL-2.1+

Files: src/tracker/*
Copyright: 2011, 2013, Intel Corporation.
 2010-2012, Igalia S.L.
License: LGPL-2.1+

Files: src/tracker/grl-tracker-request-queue.c
 src/tracker/grl-tracker-request-queue.h
 src/tracker/grl-tracker.h
Copyright: 2011, 2013, Intel Corporation.
License: LGPL-2.1+

Files: src/tracker/grl-tracker-source-notif.c
Copyright: 2015, Collabora Ltd.
 2011, Intel Corporation.
 2011, 2012, Igalia S.L.
License: LGPL-2.1+

Files: src/tracker/grl-tracker-source.h
Copyright: 2010-2012, 2014, 2018, Igalia S.L.
License: LGPL-2.1+

Files: src/tracker3/*
Copyright: 2020, Red Hat Inc.
 2011, Intel Corporation.
 2011, 2012, Igalia S.L.
License: LGPL-2.1+

Files: src/tracker3/grl-tracker-source-notif.c
Copyright: 2020, Red Hat Inc.
 2015, Collabora Ltd.
 2011, Intel Corporation.
 2011, 2012, Igalia S.L.
License: LGPL-2.1+

Files: src/tracker3/grl-tracker-source-statements.h
Copyright: 202011-2012, Igalia S.L. / 2020, Red Hat Inc / 2011, Intel Corporation.
License: LGPL-2.1+

Files: src/tracker3/grl-tracker-source.h
Copyright: 2020, Red Hat Inc.
 2011, 2012, Igalia S.L.
License: LGPL-2.1+

Files: src/tracker3/grl-tracker.c
Copyright: 2011, 2013, Intel Corporation.
 2010-2012, Igalia S.L.
License: LGPL-2.1+

Files: src/tracker3/grl-tracker.h
Copyright: 2011, 2013, Intel Corporation.
License: LGPL-2.1+

Files: src/youtube/grl-youtube.c
Copyright: 2011, 2013, Intel Corporation.
 2010-2012, Igalia S.L.
License: LGPL-2.1+

Files: src/youtube/grl-youtube.h
Copyright: 2010-2012, 2014, 2018, Igalia S.L.
License: LGPL-2.1+

Files: tests/chromaprint/*
Copyright: 2014-2016, 2018, 2019, Grilo Project
License: LGPL-2.1+

Files: tests/dleyna/*
Copyright: 2011, 2013, Intel Corporation.
License: LGPL-2.1+

Files: tests/dleyna/dbusmock/*
Copyright: 2013, Intel Corp.
License: LGPL-3+

Files: tests/games/test_games.c
Copyright: 2010-2012, 2014, 2018, Igalia S.L.
License: LGPL-2.1+

Files: tests/local-metadata/test_local_metadata.c
Copyright: 2014, 2020, Red Hat Inc.
License: LGPL-2.1+

Files: tests/lua-factory/*
Copyright: no-info-found
License: LGPL-2.1+

Files: tests/lua-factory/data/*
Copyright: 2013-2016, Victor Toso.
License: LGPL-2.1+

Files: tests/lua-factory/sources/test_lua_acoustid.c
 tests/lua-factory/sources/test_lua_theaudiodb.c
Copyright: 2014-2016, 2018, 2019, Grilo Project
License: LGPL-2.1+

Files: tests/lua-factory/test_lua_factory_source_errors.c
Copyright: no-info-found
License: LGPL-2.1+

Files: tests/steam-store/test_steam_store.c
Copyright: 2010-2012, 2014, 2018, Igalia S.L.
License: LGPL-2.1+

Files: tests/thetvdb/*
Copyright: no-info-found
License: LGPL-2.1+

Files: tests/tmdb/*
Copyright: 2012, Openismus GmbH
License: LGPL-2.1+

Files: tests/tracker3/test_tracker3.c
Copyright: 2014, 2020, Red Hat Inc.
License: LGPL-2.1+

Files: help/examples/* help/meson.build src/meson.build src/raitv/meson.build src/tracker3/meson.build tests/* tests/tracker3/* tests/tracker3/data/sample.mp3 tests/tracker3/data/sample.ogg tests/tracker3/data/sample.ogv
Copyright: © 2010-2022 The Grilo Project
License: LGPL-2.1+

Files: src/bookmarks/meson.build
Copyright: © 2010, 2011 Igalia, S.L.
 © 2014 Bastien Nocera <hadess@hadess.net>
License: LGPL-2.1+

Files: src/chromaprint/meson.build tests/chromaprint/data/* tests/chromaprint/data/sample.ogg tests/chromaprint/meson.build
Copyright: © 2016 Victor Toso <me@victortoso.com>
License: LGPL-2.1+

Files: src/dleyna/meson.build
Copyright: © 2010, 2011 Igalia, S.L.
 © 2011, 2013 Intel Corporation
 © 2014 Giovanni Campagna <scampa.giovanni@gmail.com>
License: LGPL-2.1+

Files: src/dmap/meson.build
Copyright: © 2008, 2011, 2012 W. Michael Petullo <mike@flyn.org>
 © 2012 Igalia, S.L.
License: LGPL-2.1+

Files: src/filesystem/* src/flickr/meson.build src/youtube/*
Copyright: © 2010, 2011 Igalia, S.L.
 © 2011 Intel Corporation
License: LGPL-2.1+

Files: src/freebox/meson.build
Copyright: © 2011 Intel Corporation
 © 2013 Bastien Nocera <hadess@hadess.net>
License: LGPL-2.1+

Files: src/gravatar/*
Copyright: © 2010, 2011 Igalia, S.L.
 © 2012 Canonical Ltd
License: LGPL-2.1+

Files: src/local-metadata/meson.build src/metadata-store/meson.build src/podcasts/meson.build src/shoutcast/meson.build
Copyright: © 2010, 2011 Igalia, S.L.
License: LGPL-2.1+

Files: src/lua-factory/meson.build src/lua-factory/sources/meson.build
Copyright: © 1999 Helix Code, Inc.
 © 2013 Victor Toso <me@victortoso.com>
 © 2015 Bastien Nocera <hadess@hadess.net>
License: LGPL-2.1+

Files: src/magnatune/meson.build
Copyright: © 2013 Victor Toso <me@victortoso.com>
License: LGPL-2.1+

Files: src/opensubtitles/meson.build src/optical-media/meson.build
Copyright: © 2012, 2014, 2015 Bastien Nocera <hadess@hadess.net>
License: LGPL-2.1+

Files: src/thetvdb/meson.build
Copyright: © 2014, 2016 Victor Toso <me@victortoso.com>
License: LGPL-2.1+

Files: src/tmdb/meson.build
Copyright: © 2012 Canonical Ltd
License: LGPL-2.1+

Files: src/tracker/meson.build
Copyright: © 2011-2012 Igalia S.L.
 © 2011 Intel Corporation
 © 2015 Collabora Ltd.
License: LGPL-2.1+

Files: tests/dleyna/meson.build
Copyright: © 2013 Intel Corporation
License: LGPL-2.1+

Files: tests/lua-factory/meson.build tests/lua-factory/sources/meson.build tests/thetvdb/data/* tests/thetvdb/data/boardwalk_empire_series_all_en.zip tests/thetvdb/data/csi_miami_series_all_en.zip tests/thetvdb/data/felicity_series_all_en.zip tests/thetvdb/data/house_series_all_en.zip tests/thetvdb/data/naruto_series_all_en.zip tests/thetvdb/meson.build
Copyright: © 2014, 2015, 2016 Victor Toso <me@victortoso.com>
License: LGPL-2.1+

Files: tests/tmdb/meson.build
Copyright: © 2012 Openismus GmbH
License: LGPL-2.1+
